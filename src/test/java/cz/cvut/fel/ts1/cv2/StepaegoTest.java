package cz.cvut.fel.ts1.cv2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StepaegoTest {
    @Test
    public void factorialTest() {
        Stepaego myClass = new Stepaego();
        long expectedResult = 120;
        long result = myClass.factorial(5);
        assertEquals(expectedResult, result);
    }
}
