package cz.cvut.fel.ts1.cv2;

public class Stepaego {
    public long factorial(int n){
        return n <= 1 ? 1 : factorial(n - 1);
    }
}
